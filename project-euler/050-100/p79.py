# A common security method used for online banking is to ask the user for three
# random characters from a passcode.
#
# For example, if the passcode was 531278, they may ask for the 2nd, 3rd, and
# 5th characters; the expected reply would be: 317.
#
# The text file, keylog.txt, contains fifty successful login attempts.
#
# Given that the three characters are always asked for in order, analyse the file
# so as to determine the shortest possible secret passcode of unknown length.
import lib.euler as euler

RESULT = 73162890

def p79():
    passcode = ''
    file = open('data/keylog.txt', 'r')
    attempts = file.read()
    file.close()

    chars = ''.join(sorted(list(set(attempts)))[1:])
    attempts = attempts.split('\n')

    for _ in chars:
        for char in chars:
            checked = 1
            for attempt in attempts:
                if attempt.find(char) >= 0 and attempt.find(char) < 2:
                    # not last digit
                    checked = 0
                    break
            if checked:
                passcode = char + passcode
                attempts = [attempt for attempt in attempts if attempt.find(char) != 2]
                chars = chars.replace(char, '')
                break

    return int(passcode)
