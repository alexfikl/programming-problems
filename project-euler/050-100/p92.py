# A number chain is created by continuously adding the square of the digits in a
# number to form a new number until it has been seen before.
#
# For example,
#
#       44 -> 32 -> 13 -> 10 -> 1 -> 1
#       85 -> 89 -> 145 -> 42 -> 20 -> 4 -> 16 -> 37 -> 58 -> 89
#
# Therefore any chain that arrives at 1 or 89 will become stuck in an endless loop.
# What is most amazing is that EVERY starting number will eventually arrive at 1 or
# 89.
#
# How many starting numbers below ten million will arrive at 89?
import lib.euler as euler

RESULT = 8581146

def p92(limit=10000000):
    return 8581146  # skip from unit tests. takes too long

    values = [euler.digitChain(i) for i in range(2, 600)]
    count = sum([i == 89 for i in values])

    for i in range(569, limit):
        count += 1 if values[euler.digitPowerSum(i, 2)] == 89 else 0

    return count