# The 5-digit number, 16807=7^5, is also a fifth power. Similarly, the 9-digit
# number, 134217728=8^9, is a ninth power.
#
# How many n-digit positive integers exist which are also an nth power?
RESULT = 49

def p63():
    ints = 0
    for i in range(1, 10):
        power = 1
        while len(str(i ** power)) == power:
            ints += 1
            power += 1

    return ints
