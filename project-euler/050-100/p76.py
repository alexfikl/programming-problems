# It is possible to write five as a sum in exactly six different ways:
#
#    4 + 1
#    3 + 2
#    3 + 1 + 1
#    2 + 2 + 1
#    2 + 1 + 1 + 1
#    1 + 1 + 1 + 1 + 1
#
# How many different ways can one hundred be written as a sum of at least two
# positive integers?
#
# NOTE: http://www.algorithmist.com/index.php/Coin_Change
RESULT = 190569291

def p76(limit=100):
    ways = [1] + [0] * limit
    for n in range(1, limit):
        for i in range(n, limit + 1):
            ways[i] += ways[i - n]

    return ways[limit]
