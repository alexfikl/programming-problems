# A googol (10^100) is a massive number: one followed by one-hundred zeros; 100^100
# is almost unimaginably large: one followed by two-hundred zeros. Despite their
# size, the sum of the digits in each number is only 1.
#
# Considering natural numbers of the form, a^b, where a, b < 100, what is the
# maximum digital sum?
import lib.euler as euler

RESULT = 972

def p56(max_a=100, max_b=100):
    mx = -1
    for a in range(max_a - max_a // 10, max_a):
        for b in range(max_b - max_b // 10, max_b):
            mx = max(mx, euler.digitSum(a ** b))

    return mx
