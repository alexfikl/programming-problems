'''Some common or less common functions used in euler problems.'''
import random
import math

# TODO: figure out which functions are only used once and put them in their respective
# folder
# TODO: this looks like a complete mess. figure out how to organize it better.

def divisorGen(checkLimit=None):
    '''Generates numbers to check for divisors.

    Arguments:
        checkLimit      a function that tell us when to stop.
    '''
    # TODO: there should be an easier way to do this
    if not checkLimit:
        checkLimit = lambda n: True

    yield 2
    i = 3

    while checkLimit(i):
        yield i
        i += 2

def prod(arr):
    '''Computes the product of all the elements in the array arr.

    Arguments:
        arr         array of numbers
    '''
    p = 1
    for i in arr:
        p *= i

    return p

def digitFactSum(n):
    '''Computes the sum of the factorial the digits of n.

    Arguments:
        n       number.
    '''
    return sum(math.factorial(int(i)) for i in str(n))

def combinations(n, k):
    '''Computes C_n^r as given by the formula:
            C_n^r = \frac{n!}{k!(n - k)!}

    Arguments:
        n
        k
    '''
    return math.factorial(n) / (math.factorial(k) * math.factorial(n - k))

def digitPowerSum(n, p):
    '''Computes the sum of the given power p of the digits of n.

    Arguments:
        n       number,
        p       power.
    '''
    return sum(int(i) ** p for i in str(n))


def digitSum(n):
    '''Computes sum of digits of n.

    Arguments:
        n       number.
    '''
    return sum(int(i) for i in str(n))

def charValue(c):
    '''Compute the value of a character (i.e. 'A' or 'a' is 1).

    Arguments:
        c       character to turn into number.
    '''
    return ord(c) - 64 if c.isupper() else ord(c) - 96

def alphaValue(s):
    '''Compute the value of a string (i.e. 'abc' is 1 + 2 + 3 = 6).

    Arguments:
        s       string.
    '''
    return sum(charValue(c) for c in s)

def isPalindrome(n):
    '''Check if a number is a palindrome.

    Arguments:
        n       number.
    '''
    n = str(n)
    return n == n[::-1]

def digitChain(n):
    '''Computes the number chain for a given number n and returns the number
    in which the chain ended (all chains end in either 1 or 89).

    A number chain is obtained my continuously adding the square of the
    digits in a number to form a new one until that number has been seen
    before.

    Arguments:
        n           number.
    '''
    while True:
        n = digitPowerSum(n, 2)
        if n == 1 or n == 89:
            return n

def isAbundant(n):
    '''Checks if a given number n is abundant.

    A number is abundant if the sum of its prober divisors is larger than
    the number itself.

    Arguments:
        n           number.
    '''

    if properDivisorSum(n) > n:
        return True
    return False

def isPentagonal(n):
    '''Checks if a number is a pentagonal number.

    Pentagonal numbers are obtained using the formula: P_n = \frac{n(3n - 1)}{2}.
    We can easily check if a given number is pentagonal using the formula:
        x = \frac{\sqrt(24n + 1) + 1}{6},
    so if x is a natural number n is pentagonal, more exactly it is the x-th
    pentagonal number.

    Arguments:
        n           number.
    '''
    if n < 0:
        return False

    n = (1 + (1 + 24 * n) ** 0.5) / 6
    return n == int(n)

def isTriangular(n):
    '''Checks if a number is a triangular number.

    Triangular numbers are obtained using the formula: T_n = \frac{n(n + 1)}{2}.
    We can easily check if a given number is triangular using the formula:
        x = \frac{\sqrt(8n + 1) - 1}{2},
    so if x is a natural number n is triangular, more exactly it is the x-th
    triangular number.

    Arguments:
        n           number.
    '''
    if n < 0:
        return False

    n = ((8 * n + 1) ** 0.5 - 1) / 2
    return n == int(n)

def writePrimes(filename, limit=37):
    '''Writes all the primes smaller than limit to a given file.

    Arguments:
        filename        name of the file containing primes,
        limit           upper limit of the primes list.
    '''
    with open(filename, 'w') as fd:
        for i in [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]:
            if limit < i:
                break
            fd.write("{}\n".format(i))

        for i in range(41, limit, 2):
            fd.write("{}\n".format(i)) if isPrime(i) else None

def readSmallerPrimes(filename, limit):
    '''Gets all the primes smaller than limit from a given file.

    Arguments:
        filename        name of the file containing the primes,
        limit           upper limit of primes to read.
    '''
    with open(filename, 'r') as fd:
        for line in fd:
            prime = int(line)

            if prime > limit:
                break
            yield prime

def readNPrimes(filename, n):
    '''Gets n primes from a given file.

    Arguments:
        filename    name of fil containing the primes,
        n           number of primes to read.
    '''

    with open(filename, 'r') as fd:
        for _ in range(n):
            yield int(next(fd))

def evenDigits(n):
    '''Returns the even digits of a number n.

    Arguments:
        n           number.
    '''
    return [int(i) for i in str(n) if int(i) % 2 == 0]

def checkLRPrime(n, primes=None):
    '''Check if all the numbers obtained from truncating n from left and right are
    prime.

    E.g.: for 123 check: 123, left: 23, 3, right: 12, 1.

    Arguments:
        n           number to check,
        primes      array of known primes. (optional)
    '''
    n = str(n)
    if evenDigits(n[1:]) or not isPrime(int(n), primes):
        return False

    # can start with 2 but not another even number
    if n[0] != '2' and int(n[0]) % 2 == 0:
        return False

    for i in range(1, len(n)):
        if not isPrime(int(n[i:]), primes) or not isPrime(int(n[:-i]), primes):
            return False

    return True

def checkAllRotations(n, primes=None):
    '''Checks if all circular rotations of a number are prime numbers.

    Arguments:
        n           number to check,
        primes      array of known primes. (optional)
    '''
    if n == 2 or n == 3:
        return True

    if evenDigits(n) or not isPrime(n, primes):
        return False

    n = str(n)
    for i in range(len(n)):
        n = (n + n[0])[1:]
        if not isPrime(int(n), primes):
             return False
    return True

def factorGenerator(n, primes=None):
    '''Generates all prime factors of a number as well as their multiplicity in the
    form:
        [(factor, multiplicity), ...]

    Arguments:
        n       number to factorize.
    '''
    if not primes:
        primes = list(readSmallerPrimes('data/primes.txt', n ** 0.5))

    i = 0
    while n > 1:
        c = 0
        while n % primes[i] == 0:
            c += 1
            n /= primes[i]

        if c:
            yield (primes[i], c)

        i += 1

def numberOfDivisors(n, primes):
    '''Compute the number of divisors of a number n.

    Arguments:
        n       number.
    '''
    return prod(e + 1 for d, e in factorGenerator(n, primes))

def properDivisorSum(n):
    '''Computes the sum of all the proper divisors of a number n.

    The proper divisors of a number are all the numbers smaller than it that divide
    evenly with the number (e.g.: 220 has the divisors 1, 2, 4, 5, 10, 11, 20, 22
    44, 55 and 110).

    Arguments:
        n       number.
    '''
    s = 1
    t = math.sqrt(n)
    for i in range(2, int(t) + 1):
        if n % i == 0:
            s += i + n / i
    if t == int(t):
        s -= t
    return s

def isPrime(n, primes=None):
    '''Checks if a number is prime using the classical method.

    Arguments:
        n           number to check.
        primes      array of primes. (optional)
    '''
    if not primes:
        primes = [3]

    if n == 2 or n == 3:
        return True

    if n <= 1 or n % 2 == 0 or n % 3 == 0:
        return False

    sqrtn = n ** 0.5
    for p in primes:
        if p > sqrtn:
            break
        if n % p == 0:
            return False

    # we don't have enough primes. doing the rest by hand
    if primes[-1] < sqrtn:
        for p in range(primes[-1], int(sqrtn), 2):
            if n % p == 0:
                return False

    return True

def toLetters(n):
    '''Write the number n into words (i.e 5145 is 'five thousand and one hundred and
    forty-five').

    Arguments:
        n       number to convert.

    NOTE: only converts number in [0, 9999]
    '''
    letters = {
        1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five', 6: 'six', 7: 'seven',
        8: 'eight', 9:'nine', 10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'thirteen',
        14: 'fourteen', 15: 'fifteen', 16: 'sixteen', 17: 'seventeen',
        18: 'eighteen', 19: 'nineteen', 20:'twenty', 30: 'thirty', 40: 'forty',
        50: 'fifty', 60:'sixty', 70:'seventy', 80: 'eighty', 90:'ninety', 0: ''
    }

    if n == 0:
        return 'zero'

    # we only support 4 digits so nicely pad our number
    n = [int(i) for i in str(n).rjust(4, '0')]
    n[3] = (n[2] * 10 + n[3]) if n[2] == 1 else n[3]    # numbers between [10, 20)
    n[2] = 0 if n[2] == 1 else n[2]                     # are special

    words = []

    thousand = ' thousand' if n[0] else ''
    hundred = ' hundred' if n[1] else ''
    hyphen = '-' if n[2] and n[3] else ''

    words.append('{}{}'.format(letters[n[0]], thousand))
    words.append('{}{}'.format(letters[n[1]], hundred))
    words.append('{}{}{}'.format(letters[n[2] * 10], hyphen, letters[n[3]]))

    return ' and '.join([w for w in words if w]).capitalize()

def sameDigitMultiples(n, end=6):
    '''Returns true if all the multiples of n in [2, end] contain the same digits.

    Arguments:
        n       number to check
        end     number of multiples to check
    '''
    for i in range(2, end + 1):
        if str(i * n).strip(str(n)):
            return False
    return True

def powLastDigits(base, power, digitNumber):
    '''Last digitNumber digits of base ^ power.

    Arguments:
        base        base,
        power       power,
        digitNumber (base ^ power) % (10 ** digitNumber) (last digitNumber digits).
    '''
    lastDigits = 1
    digitNumber = 10 ** digitNumber
    for i in range(power):
        lastDigits = (lastDigits * base) % digitNumber

    return lastDigits

def isPandigital(n, b=10):
    '''Check if a number is pandigital.

    A number n is pandigital in base b if it contains all the digits in the interval
    [0, b - 1] at least once.

    Arguments:
        n       number to check,
        b       base of n.
    '''
    n = str(n)
    return len(n) == b and not '1234567890'[:b].strip(n)

def isLychrel(n):
    '''Check if a given number n is a Lychrel number.

    A number is a Lychrel number if none of the numbers obtained by adding
        n + reverse(n)
    repeatedly are palindromes.

    Arguments:
        n       number to check.
    '''
    for _ in range(50):
        n = n + int(str(n)[::-1])
        if isPalindrome(n):
            return False
    return True
