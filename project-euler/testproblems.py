import os, sys
import re
import unittest

class TestProblems(unittest.TestCase):
    def check_something(self, fct, res):
        self.assertEqual(fct(), res)

def addTest(name, fct, res):
    def testProblem(self):
        self.check_something(fct, res)

    setattr(TestProblems, 'test' + name, testProblem)
    testProblem.__name__ = 'test' + name

def addProblems(folder, files):
    re_file = re.compile("^(p\d+)\.py$")
    for fname in files:
        match = re_file.match(fname)
        if match:
            problem = match.group(1)
            pb = __import__("{}.{}".format(folder, problem), fromlist=[problem])
            pbFct = getattr(pb, problem, None)
            pbResult = getattr(pb, 'RESULT', 0)
            print(pbFct, pbResult)
            addTest(problem, pbFct, pbResult)

def setUp():
    # TODO: this is probably bad. only works when called with:
    #       python -m unittest -v testproblems.py pXX.py
    print(len(sys.argv))
    if len(sys.argv) <= 3:
        folders = ["001-050", "050-100"]
        for folder in folders:
            addProblems(folder, os.listdir(folder))
    else:
        addProblems(sys.argv[1:])

setUp()

if __name__ == "__main__":
    unittest.main()
