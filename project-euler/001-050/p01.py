# If we list all the natural numbers below 10 that are multiples of 3 or 5, we get
# 3, 5, 6 and 9. The sum of these multiples is 23.
#
# Find the sum of all the multiples of 3 or 5 below 1000.
RESULT = 233168

def p01(n=1000):
    r = 1.5 * ((n - 1) // 3) * ((n + 2) // 3)       # multiples of 3
    r += 2.5 * ((n - 1) // 5) * ((n + 4) // 5)      # multiples of 5
    r -= 7.5 * ((n - 1) // 15) * ((n + 14) // 15)   # substract multiples of 15

    return int(r)
