#The following iterative sequence is defined for the set of positive integers:
#
#    n -> n/2 (n is even)
#    n -> 3n + 1 (n is odd)
#
# Using the rule above and starting with 13, we generate the following sequence:
#
#    13 ->  40 ->  20 ->  10 ->  5 ->  16 ->  8 ->  4 ->  2 ->  1
# It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms.
# Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
#
# Which starting number, under one million, produces the longest chain?
#
# NOTE: Once the chain starts the terms are allowed to go above one million.
import lib.euler as euler

RESULT = 837799

ldict = {1: 1}

def stepsFor(n):
    '''Computes the number of steps necessary for the Collatz function
    to reach 1 for the number n.

    The Collatz function is:
        C(n) =  | n/2       n even
                | 3n + 1    otherwise
    and is conjectured to always reach 1 after repeated iterations.

    Arguments:
        n       number to use,
        ldict   dictionary of previously computed numbers. (optional)
    '''
    if n not in ldict:
        ldict[n] = stepsFor(3 * n + 1 if n % 2 else n // 2) + 1
    return ldict[n]

def p14(limit=1000000):
    mx = (0, 0)

    for i in range(3, limit, 2):
        result = stepsFor(i)
        if result > mx[1]:
            mx = (i, result)

    return mx[0]

