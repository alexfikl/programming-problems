# 2520 is the smallest number that can be divided by each of the numbers from 1 to
# 10 without any remainder.
#
# What is the smallest positive number that is evenly divisible by all of the numbers
# from 1 to 20?
RESULT = 232792560

def p05(n=20):
    '''TODO: make this work for other values.

    NOTE: given all the prime numbers p smaller than n. find the biggest
    power q of each p such that p^q < n.'''
    return (2 ** 4) * (3 ** 2) * 5 * 7 * 11 * 13 * 17 * 19
