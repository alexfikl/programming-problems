# An irrational decimal fraction is created by concatenating the positive integers:
#
#    0.123456789101112131415161718192021...
#
# It can be seen that the 12th digit of the fractional part is 1.
#
# If d_n represents the nth digit of the fractional part, find the value of the
# following expression.
#
#    d_1 x d_10 x d_100 x d_1000 x d_10000 x d_100000 x d_1000000
import lib.euler as euler

RESULT = 210

def p40(step=10):
    d = ['1']
    length = 1
    number = 2

    while length < 1000000:
        d.append(str(number))
        length += len(str(number))
        number += 1

    d = ''.join(d)

    return euler.prod([int(d[i - 1]) for i in [step ** j for j in range(7)]])
