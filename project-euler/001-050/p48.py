# The series, 1^1 + 2^2 + 3^3 + ... + 10^10 = 10405071317.
#
# Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000.
RESULT = 9110846700

def p48(d=10):
    return sum([i ** i for i in range(1, 1001)]) % 10 ** d
