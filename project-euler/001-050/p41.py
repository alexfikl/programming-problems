# We shall say that an n-digit number is pandigital if it makes use of all the
# digits 1 to n exactly once. For example, 2143 is a 4-digit pandigital and is also
# prime.
#
# What is the largest n-digit pandigital prime that exists?
import lib.euler as euler

RESULT = 7652413

def p41():
    # all {5, 6, 8, 9}-pandigital numbers are divisible by 3 so not primes
    # that that leaves 4 and 7 and we're checking 7 because it's bigger
    for i in range(7654321, 1234567, -2):
        if euler.isPrime(i) and euler.isPandigital(i, 7):
            return i
