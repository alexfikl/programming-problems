# Using a file containing first names, begin by sorting it into alphabetical order.
# Then working out the alphabetical value for each name, multiply this value by its
#alphabetical position in the list to obtain a name score.
#
# For example, when the list is sorted into alphabetical order, COLIN, which is
# worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would
# obtain a score of 938 x 53 = 49714.
#
# What is the total of all the name scores in the file?
import lib.euler as euler

RESULT = 871198282

def p22():
    ind = 1
    s = 0
    with open('data/names.txt', 'r') as fd:
        for line in fd:
            names = sorted(line.replace('"', '').split(','))
            s += sum([(ind + i) * euler.alphaValue(n) for i, n in enumerate(names)])
            ind += len(names)

    return s