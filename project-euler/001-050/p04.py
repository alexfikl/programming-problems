# Problem: p04.py
#
# 	 >> largest palindrome: 906609
# 	 >> took 0.043s
#
# A palindromic number reads the same both ways.
# The largest palindrome made from the product of two 2-digit numbers is 9009 = 91x99.
#
# Find the largest palindrome made from the product of two 3-digit numbers.
import lib.euler as euler

RESULT = 906609

def p04(d=3):
    i = 10 ** d - 1
    mx = -1
    while i > 10 ** (d - 1):
        j = 10 ** d - 1
        while j > i:
            if i * j <= mx:
                break
            if euler.isPalindrome(i * j):
                mx = i * j
            j -= 1
        i -= 1

    return mx

