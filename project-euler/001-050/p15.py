# Starting in the top left corner of a 2x2 grid, there are 6 routes (without backtracking) to the bottom right corner.
#
# How many routes are there through a 20x20 grid?
import math

RESULT = 137846528820

def p15(n=20):
    return math.factorial(2 * n) // (math.factorial(n) ** 2)
