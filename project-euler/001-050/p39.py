# If p is the perimeter of a right angle triangle with integral length sides,
# {a, b, c}, there are exactly three solutions for p = 120.
#
#       {20,48,52}, {24,45,51}, {30,40,50}
#
# For which value of p <= 1000, is the number of solutions maximised?
RESULT = 840

def p39(limit=1000):
    triangles_max = 0
    perimeter_max = 0

    for p in range(2, limit + 1, 2):
        triangles = 0
        for a in range(2, p // 4 + 1):
            if p * (p - 2 * a) % (2 * (p - a)) == 0:
                triangles += 1
        if triangles > triangles_max:
            triangles_max = triangles
            perimeter_max = p

    return perimeter_max
