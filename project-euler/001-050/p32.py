# We shall say that an n-digit number is pandigital if it makes use of all the
# digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1 through
# 5 pandigital.
#
# The product 7254 is unusual, as the identity, 39 x 186 = 7254, containing
# multiplicand, multiplier, and product is 1 through 9 pandigital.
#
# Find the sum of all products whose multiplicand/multiplier/product identity can be
# written as a 1 through 9 pandigital.
#
# HINT: Some products can be obtained in more than one way so be sure to only include
# it once in your sum.
#
# TODO: check if this works for other values
import lib.euler as euler

RESULT = 45228

def p32(d=9):
    p = set()
    for i in range(2, 100):
        start = 1234
        if i > 9:
            start = 123
        for j in range(start, 10000 // i + 1):
            if euler.isPandigital(''.join([str(j) for j in [i, j, i * j]]), d):
                p.add(i * j)

    return sum(p)
