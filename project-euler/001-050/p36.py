# The decimal number, 585 = 1001001001_2 (binary), is palindromic in both bases.
#
# Find the sum of all numbers, less than one million, which are palindromic in base
# 10 and base 2.
#
# (Please note that the palindromic number, in either base, may not include leading
# zeros.)
import lib.euler as euler

RESULT = 872187

def p36(limit=1000000):
    s = 0

    # even numbers have a zero at the end
    for i in range(1, limit, 2):
        if euler.isPalindrome(i) and euler.isPalindrome(bin(i)[2:]):
            s += i

    return s
