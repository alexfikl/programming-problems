# A permutation is an ordered arrangement of objects. For example, 3124 is one
# possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are
# listed numerically or alphabetically, we call it lexicographic order. The
# lexicographic permutations of 0, 1 and 2 are:
#
#    012   021   102   120   201   210
#
# What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5,
# 6, 7, 8 and 9?
import itertools

RESULT = 2783915460

def p24(n=1000000):
    perms = itertools.permutations(range(10))
    for _ in range(n):
        pg = next(perms)

    return int("".join([str(i) for i in pg]))

