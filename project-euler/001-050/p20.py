# n! means n x (n - 1) x ... x 3 x 2 x 1
#
# Find the sum of the digits in the number 100!
import math

RESULT = 648

def p20(n=100):
    return sum([int(i) for i in str(math.factorial(100))])
