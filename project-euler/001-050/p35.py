# The number 197 is called a circular prime because all rotations of the digits:
# 197, 971, and 719, are themselves prime.
#
# There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71,
# 73, 79, and 97.
#
# How many circular primes are there below one million?
import lib.euler as euler

RESULT = 55

def p35(limit=1000000):
    primes = list(euler.readSmallerPrimes('data/primes.txt', limit))
    counter = 0

    for p in primes:
        if euler.checkAllRotations(p, primes):
            counter += 1

    return counter
