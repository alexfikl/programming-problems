# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
#
# What is the 10001st prime number?
import lib.euler as euler

RESULT = 104743

def p07(n=10001):
    return list(euler.readNPrimes('data/primes.txt', n))[-1]
