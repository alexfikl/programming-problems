# The prime factors of 13195 are 5, 7, 13 and 29.
#
# What is the largest prime factor of the number 600851475143 ?
import lib.euler as euler

RESULT = 6857

def p03(n=600851475143):
    factors = euler.divisorGen()
    while n > 1:
        f = next(factors)
        while n % f == 0:
            n = n / f

    return f
