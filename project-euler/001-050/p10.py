# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
# Find the sum of all the primes below two million.
import lib.euler as euler

RESULT = 142913828922

def p10(limit=2000000):
    return sum(euler.readSmallerPrimes('data/primes.txt', limit))
