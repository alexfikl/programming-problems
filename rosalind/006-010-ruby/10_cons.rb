require './utils'

def profile_matrix(data)
    pos = { 'A' => 0, 'C' => 1, 'G' => 2, 'T' => 3 }

    profile = Array.new(4) { Array.new(data[0].length) { 0 } }
    data.each do |row|
        row.each_index do |col|
            profile[pos[row[col]]][col] += 1
        end
    end

    profile
end

def consensus(profile)
    pos = { 0 => 'A', 1 => 'C', 2 => 'G', 3 => 'T' }
    cons = []
    profile[0].each_index do |col|
         cons << profile.each_with_index.max { |a, b| a[0][col] <=> b[0][col] }[1]
    end

    cons.collect! { |x| pos[x]}
end

datafile = 'datasets/cons.txt'

data = File.readlines(datafile).collect { |line| line.strip.split('') }

profile = profile_matrix(data)
cons = consensus(profile)
pparray('%c', cons, '')
ppmatrix(profile, ['T: ', 'G: ', 'C: ', 'A: '])
