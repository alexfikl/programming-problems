require './utils'

filename = 'datasets/perm.txt'

n = Integer(File.read(filename))
permutations = (1..n).to_a.permutation.to_a

p permutations.length
ppmatrix(permutations, Array.new(n) { '' })
