def fibk_acc(n, k, a, b)
    if n <= 0
        return a
    end

    return fibk_acc(n - 1, k, b, b + k * a)
end

def fibk(n, k)
    fibk_acc(n, k, 0, 1)
end

p fibk(5, 3)
p fibk(33, 4)
p fibk(33, 5)
