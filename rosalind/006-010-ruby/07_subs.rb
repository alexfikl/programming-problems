require './utils'

def get_location(s, subs)
    (1..s.length + 1).select do |i|
        s[i - 1..-1].start_with? subs
    end
end

datafile = 'datasets/subs.txt'

data = File.readlines(datafile).collect { |line| line.strip }

pparray('%d', get_location(data[0], data[1]), ' ')
