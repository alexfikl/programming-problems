require './utils'

def encode(dna, table)
    encoded_dna = ''
    dna.scan(/.../).each do |acid|
        break if table[acid] == 'Stop'
        encoded_dna << table[acid]
    end

    encoded_dna
end

datafile = 'datasets/prot.txt'
tablefile = 'datasets/codon_table.txt'

rna_codon_table = get_codon_table(tablefile)

dna = File.read(datafile)

puts encode(dna, rna_codon_table)
