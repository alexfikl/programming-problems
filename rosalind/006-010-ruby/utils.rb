def pparray(fmt, array, sep)
    puts array.collect { |x| fmt % x }.join(sep)
end

def ppmatrix(matrix, rowlabel = ['', '', '', ''])
    matrix.each do |row|
        print rowlabel.pop, row.each { |e| e.to_s }.join(" ")
        puts ""
    end
end

def get_codon_table(filename)
    table = Hash.new { '' }

    File.readlines(filename).each do |line|
        line = line.rstrip.split ' '
        code = line[0]

        line[1..-1].each { |acid| table[acid] = code }
    end

    table
end
