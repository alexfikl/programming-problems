require './utils'

def probability(array)
    array.collect { |x|
        pc = x / 2
        pa = (1 - x) / 2

        2 * (pc ** 2 + pa ** 2)
    }
end

filename = 'datasets/prob.txt'

a = File.read(filename).split(' ').collect { |x| x.to_f }

pparray("%1.5f", probability(a), ' ')
