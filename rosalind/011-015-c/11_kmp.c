#include <stdio.h>
#include <stdlib.h>

#include "utils.h"

#define MAXDNASIZE 100000

const char *filename = "datasets/kmp.txt";

int* get_failure_array(char *dna)
{
    int i, k, n;
    int *p;

    n = strlen(dna);
    p = (int *)calloc(n, sizeof(int));

    k = 0;
    for(i = 1; i < n; ++i)
    {
        while(k > 0 && dna[k] != dna[i])
            k = p[k - 1];

        if(dna[k] == dna[i])
            ++k;

        p[i] = k;
    }

    return p;
}

int main(void)
{
    FILE *fd;
    char dna[MAXDNASIZE];

    fd = fopen(filename, "r");
    fscanf(fd, "%s", dna);
    fclose(fd);

    int *p = get_failure_array(dna);
    pparray(int, p, strlen(dna));

    free(p);

    return 0;
}
