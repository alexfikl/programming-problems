#ifndef __UTILS_H__
#define __UTILS_H__

#include <string.h>

#define pparray(TYPE, A, n) pparray_ ## TYPE(A, n);

/*
 * Pretty Print Array
 */
void pparray_int(int *a, int n);
void pparray_double(double *a, int n);

/*
 * FASTA format reader
 */
typedef struct fastaentry fastaentry_t;
typedef struct fastareader fastareader_t;

struct fastaentry {
    char *id;
    char *dna;
    fastaentry_t *next;
};

struct fastareader {
    char *file;
    fastaentry_t *entries;
};

fastareader_t* init_reader(const char *filename);
void read_entries(fastareader_t *reader);
void write_entries(const fastareader_t *reader, FILE *fd);
void free_reader(fastareader_t *reader);

/*
 * Compare slices of two strings
 */

int cmp_slice(const char *str1, const int start1, const int end1,
                const char *str2, const int start2, const int end2);

#endif //__UTILS_H__
