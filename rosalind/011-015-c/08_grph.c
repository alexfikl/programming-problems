#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "utils.h"

#define OVERLAPLENGTH 3

const char *filename = "datasets/grph.txt";

bool cmp_dna(const char *dna1, const char *dna2)
{
    int i;
    int n = strlen(dna1) - 1;
    int m = strlen(dna2) - 1;

    if(n < OVERLAPLENGTH || m < OVERLAPLENGTH)
        return false;

    for(i = 0; i < OVERLAPLENGTH; ++i)
        if(dna1[n - i] != dna2[OVERLAPLENGTH - i - 1])
            return false;

    return true;
}

void get_overlaps(fastareader_t *reader)
{
    fastaentry_t *it = reader->entries;
    fastaentry_t *jt;

    while(it)
    {
        jt = reader->entries;
        while(jt)
        {
            if(it != jt && cmp_dna(it->dna, jt->dna))
                printf("%s %s\n", it->id, jt->id);

            jt = jt->next;
        }

        it = it->next;
    }
}

int main(void)
{
    fastareader_t *reader = init_reader(filename);

    read_entries(reader);
    get_overlaps(reader);

    free_reader(reader);

    return 0;
}
