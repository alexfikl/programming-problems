#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "utils.h"

const char *filename = "datasets/eval.txt";

double* expected_mean(int n, int m, double *gc, int gc_size)
{
    int i;
    double *emean, p, q;

    emean = (double *) malloc(sizeof(double) * gc_size);
    for(i = 0; i < gc_size; ++i)
    {
        p = gc[i] / 2;
        q = (1 - gc[i]) / 2;
        emean[i] = (n - m + 1) * pow(2 * (p * p + q * q), m);
    }

    return emean;
}

int main(void)
{
    FILE *fd;
    int n, m, i = 0;
    double *gc;

    fd = fopen(filename, "r");
    gc = (double *)malloc(sizeof(double) * 20);

    fscanf(fd, "%d %d", &m, &n);

    while(!feof(fd))
    {
        fscanf(fd, "%lf", &gc[i++]);
    }
    i = i - 1;

    double *emean = expected_mean(n, m, gc, i);
    pparray(double, emean, i);

    free(emean);
    free(gc);
    fclose(fd);

    return 0;
}

