#include <stdio.h>
#include <stdlib.h>

#include "utils.h"

#define MAXDNASIZE 16384
#define MAXIDSIZE 256

void pparray_int(int *a, int n)
{
    int i;
    for(i = 0; i < n; ++i)
        printf("%d ", a[i]);
    printf("\n");
}

void pparray_double(double *a, int n)
{
    int i;
    for(i = 0; i < n; ++i)
        printf("%g ", a[i]);
    printf("\n");
}

void add_entry(fastareader_t *reader, const char *id, const char *dna)
{
    int length = strlen(dna) - 1;
    fastaentry_t *newentry;

    newentry = (fastaentry_t *)malloc(sizeof *newentry);
    newentry->id = strdup(id);
    newentry->dna = strdup(dna);
    newentry->next = NULL;

    /* remove the newline at the end */
    newentry->dna[length] = '\0';

    if(reader->entries == NULL)
        reader->entries = newentry;
    else
    {
        newentry->next = reader->entries;
        reader->entries = newentry;
    }
}

void remove_entry(fastaentry_t *e)
{
    free(e->id);
    free(e->dna);
    free(e);
}

fastareader_t* init_reader(const char *filename)
{
    fastareader_t *reader;
    reader = (fastareader_t *)malloc(sizeof *reader);

    if(!reader)
        return NULL;

    reader->file = strdup(filename);
    reader->entries = NULL;

    return reader;
}

void read_entries(fastareader_t *reader)
{
    FILE *fd;
    int i;
    char id[MAXIDSIZE];
    char buf[MAXDNASIZE];

    fd = fopen(reader->file, "r");

    /* TODO: dna string can span multiple lines */
    /* TODO: handle EOL at EOF */
    while(!feof(fd))
    {
        fgets(buf, MAXIDSIZE, fd);
        sscanf(buf, ">%s",id);

        fgets(buf, MAXDNASIZE, fd);

        add_entry(reader, id, buf);
    }

    fclose(fd);
}

void write_entries_rec(const fastaentry_t *e, FILE *fd)
{
    if(e->next)
        write_entries_rec(e->next, fd);

    fprintf(fd, ">%s\n%s\n", e->id, e->dna);
}

void write_entries(const fastareader_t *reader, FILE *fd)
{
    write_entries_rec(reader->entries, fd);
}

void free_reader(fastareader_t *reader)
{
    fastaentry_t *it = reader->entries;
    fastaentry_t *tmp;

    while(it)
    {
        tmp = it;
        it = it->next;
        remove_entry(tmp);
    }

    free(reader->file);
    free(reader);
}

int cmp_slice(const char *str1, const int start1, const int end1,
              const char *str2, const int start2, const int end2)
{
    int i;
    int n = strlen(str1);
    int m = strlen(str2);

    if(start1 > end1 || end1 > n || n == 0)
        return 0;

    if(start2 > end2 || end2 > m || m == 0)
        return 0;

    if((end1 - start1) != (end2 - start2))
        return 0;

    for(i = 0; i < (end1 - start1); ++i)
    {
        if(str1[start1 + i] != str2[start2 + i])
            return 0;
    }

    return 1;
}
