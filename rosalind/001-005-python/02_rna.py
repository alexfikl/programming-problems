def transcribe(dna):
    dna = dna.upper()

    if any(i for i in dna if i not in "ACGT"):
        raise ValueError("DNA nucleotides are formed only from A, C, G or T.")

    return dna.replace('T', 'U')


if __name__ == "__main__":
    filename = 'datasets/rna.txt'
    data = open(filename, 'r').read().replace("\n", '')

    print(transcribe(data))
