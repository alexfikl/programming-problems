def hamming_distance(dna1, dna2):
    return sum([dnas1 != dna2[i] for i, dnas1 in enumerate(dna1)])


if __name__ == "__main__":
    filename = 'datasets/hamm.txt'
    data = open(filename, 'r').read().split('\n')

    print(hamming_distance(data[0], data[1]))
