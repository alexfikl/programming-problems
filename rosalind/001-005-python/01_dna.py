def count_nucleotides(dna):
    dna = dna.upper()

    if any(i for i in dna if i not in "ACGT"):
        raise ValueError("DNA nucleotides are formed only from A, C, G or T.")

    return [dna.count('A'), dna.count('C'), dna.count('G'), dna.count('T')]


if __name__ == "__main__":
    filename = 'datasets/dna.txt'
    data = open(filename, 'r').read().replace("\n", '')

    print(" ".join([str(i) for i in count_nucleotides(data)]))
