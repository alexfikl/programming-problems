def gc_get_data(filename):
    dna_data = []
    with open(filename, 'r') as fd:
        for line in fd:
            line = line[:-1]
            if line.startswith('>'):
                dna_data.append([line[1:], ''])
            else:
                dna_data[-1][1] += line

    return dna_data


def gc_content(filename):
    def gc_content(x):
        return (x.count('C') + x.count('G')) / len(x)

    dna_data = gc_get_data(filename)

    max_id = 0
    max_gc = 0

    for did, dna in dna_data:
        if max_gc < gc_content(dna):
            max_gc = gc_content(dna)
            max_id = did

    return (max_id, max_gc)


if __name__ == "__main__":
    filename = 'datasets/gc.txt'

    result = gc_content(filename)
    print("{}\n{:.7%}".format(result[0], result[1]))
