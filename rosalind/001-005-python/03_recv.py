def reverse_complement(dna):
    dna = dna.upper()
    revn = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C'}

    if any(i for i in dna if i not in "ACGT"):
        raise ValueError("DNA nucleotides are formed only from A, C, G or T.")

    return [revn[i] for i in dna][::-1]


if __name__ == "__main__":
    filename = 'datasets/revc.txt'
    data = open(filename, 'r').read()

    print("".join(reverse_complement(data)))
