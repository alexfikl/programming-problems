use std::cmp::max;

fn collatz(n: int) -> int {
    if n == 1 {
        return 1;
    }

    if n % 2 == 1 {
        1 + collatz(3 * n + 1)
    } else {
        1 + collatz(n / 2)
    }
}

fn main() {
    let i: int = 1;
    let j: int = 10;
    let mut max_cycle: int = 0;

    for n in range(i, j) {
        max_cycle = max(max_cycle, collatz(n));
    }

    println!("The longest cycle is {} iterations long.", max_cycle);
}
