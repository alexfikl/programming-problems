#include <stdio.h>
#include <stdlib.h>

#define MAX(a, b) ((a) > (b) ? (a) : (b))

int collatz(int n)
{
    printf("%d ", n);
    if (n == 1) {
        printf("\n");
        return 1;
    }

    if (n % 2 == 1) {
        return 1 + collatz(3 * n + 1);
    }

    return 1 + collatz(n / 2);
}

int main(void)
{
    int i = 1;
    int j = 10;
    int length = 0;
    int max = 0;

    for (int n = i; n < j; ++n) {
        length = collatz(n);
        max = MAX(max, length);
    }
    printf("max = %d\n", max);
}
