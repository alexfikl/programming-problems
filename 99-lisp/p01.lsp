;;; P01. Get the last element of a array
(defun list-last (array)
  (if (not (rest array))
    array
    (list-last (rest array))))

;;; P02. Get the second last element of a array
(defun array-but-last (array)
  (last (butlast array)))

;;; P03. Get the kth element from a list
(defun list-element-at (array k)
  (if (= k 1)
    (car array)
    (list-element-at (rest array) (- k 1))))

;;; P04. Get the size of a list
(defun list-size (array)
  (list-size-acc array 0))

(defun list-size-acc (array n)
  (if (not (car array))
    n
    (list-size-acc (rest array) (+ n 1))))

;;; P05. Reverse a list
(defun list-reverse (array)
  (list-reverse-acc array '()))

(defun list-reverse-acc (array revarray)
  (if (car array)
    (list-reverse-acc (rest array) (append (list (car array)) revarray))
    revarray))

;;; P06. Check if a list is a palindrome
(defun list-is-palindrome (array)
  (list-equal array (list-reverse array)))

(defun list-equal (list1 list2)
  (cond
    ;; if the lists are different sizes they're not equal
    ((/= (list-size list1) (list-size list2)) nil)
    ;; if we're at the end of both lists they're equal
    ((and (not (rest list1)) (not (rest list2))) t)
    ;; if the elements are not equal the lists are not equal
    ((/= (car list1) (car list2)) nil)
    ;; go to the next elements
    (t (list-equal (rest list1) (rest list2)))))

;;; P07. Flatten a nested list
;;; TODO: this is O(n^2) if i'm not mistaken. bad! bad! bad!
(defun list-flatten (array)
  (cond
    ((null array) array)
    ((atom array) (list array))
    (t (append (list-flatten (first array)) (list-flatten (rest array))))))

;;; P08. Remove duplicate consecutive elements of a list
(defun list-compress (array)
  (list-compress-acc array '()))

(defun list-compress-acc (array result)
  (cond
    ((null array) (list-reverse result))
    ((eq (first array) (first result)) (list-compress-acc (rest array) result))
    (t (list-compress-acc (rest array) (append (list (first array)) result)))))

;;; P09. Pack consecutive duplicates into sublists
(defun list-pack (array)
  (let ( (result (list (list (first array)))) )
    (dolist (item (rest array) result)
      (cond
        ((eq item (first (first (last result)))) (push item (car (last result))))
        (t (setf result (append result (list (list item)))))))))

;;; P10. Run length encoding
(defun encode (array)
  (encode-packed (list-pack array)))

(defun encode-packed (array)
  (if (null array)
    array
    (append (list (list (length (first array)) (first (first array))))
            (encode-packed (rest array)))))

;;; P11. Modify P10 so that if an element has no duplicates it is copied normally
;;; into the array.
(defun encode-mod (array)
  (encode-pmod (list-pack array)))

(defun encode-pmod (array)
  (cond
    ((null array) array)
    ((= 1 (length (first array))) (append (list (first (first array))) (encode-pmod (rest array))))
    (t (append (list (list (length (first array)) (first (first array))))
            (encode-pmod (rest array))))))

;;; kate: indent-mode LISP; indent-width 2; tab-width 2;